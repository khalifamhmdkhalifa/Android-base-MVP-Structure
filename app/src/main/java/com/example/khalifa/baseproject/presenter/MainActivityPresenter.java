package com.example.khalifa.baseproject.presenter;

import com.example.khalifa.baseproject.model.interactor.MainInterActor;
import com.example.khalifa.baseproject.view.interfaces.MainView;

public abstract class MainActivityPresenter extends
        BaseActivityPresenter<MainView, MainInterActor> {

    public MainActivityPresenter(MainView view) {
        super(view);
    }
}
