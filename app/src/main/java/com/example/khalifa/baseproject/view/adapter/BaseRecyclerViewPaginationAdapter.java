package com.example.khalifa.baseproject.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public abstract class BaseRecyclerViewPaginationAdapter<T extends Object,
        H extends RecyclerView.ViewHolder> extends BaseRecyclerViewAdapter<T, H> {

    protected int itemsInPage;

    public BaseRecyclerViewPaginationAdapter(Context context, List<T> items, int itemsInPage) {
        super(context, items);
        this.itemsInPage = itemsInPage;
    }

    @Override
    public void onBindViewHolder(H holder, int position) {
        super.onBindViewHolder(holder, position);
        if (getItemCount() == position - 1)
            loadNextPage((getItemCount() / itemsInPage) + 1);
    }

    protected abstract void loadNextPage(int nextPageNumber);
}
