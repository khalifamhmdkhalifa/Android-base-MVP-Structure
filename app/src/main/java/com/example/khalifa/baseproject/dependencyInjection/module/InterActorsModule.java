package com.example.khalifa.baseproject.dependencyInjection.module;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.model.interactor.MainInterActor;
import com.example.khalifa.baseproject.model.interactor.MainInterActorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class InterActorsModule {

    @Singleton
    @Provides
    MainInterActor provideMainInterActor(CustomApplication customApplication) {
        MainInterActorImpl interactor = new MainInterActorImpl();
        customApplication.getApplicationComponent().inject(interactor);
        return interactor;
    }
}
