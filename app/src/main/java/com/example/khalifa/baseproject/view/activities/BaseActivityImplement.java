package com.example.khalifa.baseproject.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.dependencyInjection.component.ActivityComponent;
import com.example.khalifa.baseproject.dependencyInjection.component.ApplicationComponent;
import com.example.khalifa.baseproject.presenter.BaseActivityPresenter;
import com.example.khalifa.baseproject.view.interfaces.BaseView;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseActivityImplement<P extends BaseActivityPresenter,
        C extends ActivityComponent>
        extends AppCompatActivity implements BaseView {
    @Inject
    P presenter;
    @Inject
    ActivityNavigator activityNavigator;
    private C activityComponent;
    private View loadingView;


    protected P getPresenter() {
        return presenter;
    }

    protected C getActivityComponent() {
        if (activityComponent == null)
            activityComponent = initComponent();
        return activityComponent;
    }

    abstract C initComponent();

    abstract void inject();

    protected abstract int getLoadingViewId();

    protected abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        inject();
        ButterKnife.bind(this);
        loadingView = findViewById(getLoadingViewId());
        getPresenter().onCreate();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getPresenter().onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().onDestroy();

    }


    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().onPause();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showNoInternetView() {
    }

    protected ActivityNavigator getActivityNavigator() {
        return activityNavigator;
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((CustomApplication) getApplication())
                .getApplicationComponent();
    }
}