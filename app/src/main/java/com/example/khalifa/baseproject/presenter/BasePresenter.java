package com.example.khalifa.baseproject.presenter;

import android.content.Intent;

import com.example.khalifa.baseproject.model.Response.BaseResponse;
import com.example.khalifa.baseproject.model.interactor.BaseInterActor;
import com.example.khalifa.baseproject.view.interfaces.BaseResponseViewSubscriber;
import com.example.khalifa.baseproject.view.interfaces.BaseView;
import com.example.khalifa.baseproject.view.interfaces.BaseViewSubscriber;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public abstract class BasePresenter<V extends BaseView, I extends BaseInterActor> {
    private V view;
    @Inject
    I interActor;

    private CompositeDisposable subscriptions;

    protected I getInterActor() {
        return interActor;
    }

    public BasePresenter(V view) {
        this.view = view;
        subscriptions = new CompositeDisposable();
    }

    protected V getView() {
        return view;
    }

    public abstract void onCreate();

    public abstract void onStart();

    public abstract void onResume();

    public abstract void onPause();

    public abstract void onStop();


    public void onDestroy() {
        unsubscribeAll();
        view = null;
    }

    protected <T extends Object> Disposable subscribeObservable(
            BaseViewSubscriber<T> baseViewSubscriber,
            Observable<T> observable) {
        return baseViewSubscriber.subscribeObservable(observable);
    }


    protected <T extends BaseResponse> Disposable subscribeObservable(
            BaseResponseViewSubscriber<T> baseViewSubscriber,
            Observable<T> observable) {
        return baseViewSubscriber.subscribeObservable(observable);
    }


    private void unsubscribeAll() {
        subscriptions.dispose();
    }

    protected void addSubscribe(Disposable subscriber) {
        subscriptions.add(subscriber);
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    public abstract void onActivityResult(int requestCode, int resultCode, Intent data);
}