package com.example.khalifa.baseproject.model.interactor;

import javax.inject.Inject;

import retrofit2.Retrofit;

public abstract class BaseNetworkInterActor extends BaseInterActor {
    @Inject
    Retrofit retrofit;

    protected Retrofit getRetroFit() {
        return retrofit;
    }
}