package com.example.khalifa.baseproject.dependencyInjection.module;

import android.app.Activity;

import com.example.khalifa.baseproject.dependencyInjection.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
class BaseActivityModule {
    private final Activity activity;

    public BaseActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity provideActivity() {
        return activity;
    }
}
