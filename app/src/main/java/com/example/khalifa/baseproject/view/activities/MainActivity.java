package com.example.khalifa.baseproject.view.activities;

import android.os.Bundle;

import com.example.khalifa.baseproject.R;
import com.example.khalifa.baseproject.dependencyInjection.component.DaggerMainActivityComponent;
import com.example.khalifa.baseproject.dependencyInjection.component.MainActivityComponent;
import com.example.khalifa.baseproject.dependencyInjection.module.MainActivityModule;
import com.example.khalifa.baseproject.presenter.MainActivityPresenter;
import com.example.khalifa.baseproject.view.interfaces.MainView;

public class MainActivity extends
        BaseActivityImplement<MainActivityPresenter, MainActivityComponent> implements MainView {


    @Override
    MainActivityComponent initComponent() {
        return DaggerMainActivityComponent.builder().mainActivityModule(
                new MainActivityModule(this))
                .applicationComponent(getApplicationComponent()).build();
    }

    @Override
    void inject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLoadingViewId() {
        return 0;
    }

    @Override
    protected int getLayoutId() {
        return (R.layout.activity_main);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void finishActivity() {
        finish();
    }
}