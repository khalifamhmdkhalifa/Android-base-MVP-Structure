package com.example.khalifa.baseproject.dependencyInjection.module;

import android.app.Activity;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.dependencyInjection.PerActivity;
import com.example.khalifa.baseproject.presenter.MainActivityPresenter;
import com.example.khalifa.baseproject.presenter.MainActivityPresenterImpl;
import com.example.khalifa.baseproject.view.interfaces.MainView;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule extends BaseActivityModule {

    public MainActivityModule(Activity activity) {
        super(activity);
    }

    @Provides
    @PerActivity
    MainView provideMainView(Activity activity) {
        return (MainView) activity;
    }

    @Provides
    @PerActivity
    MainActivityPresenter provideMainActivityPresenter(
            MainView mainView, CustomApplication application) {
        MainActivityPresenterImpl mainActivityPresenter = new MainActivityPresenterImpl(mainView);
        application.getApplicationComponent().inject(mainActivityPresenter);
        return mainActivityPresenter;
    }
}
