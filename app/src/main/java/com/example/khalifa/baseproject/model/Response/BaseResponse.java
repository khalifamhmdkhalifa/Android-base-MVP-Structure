package com.example.khalifa.baseproject.model.Response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BaseResponse<T extends Object> {
    @SerializedName("Data")
    private T data;
    @SerializedName("Errors")
    private ArrayList<BaseError> errors;

    @SerializedName("Result")
    private boolean result;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ArrayList<BaseError> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<BaseError> errors) {
        this.errors = errors;
    }

    public boolean isValidResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}