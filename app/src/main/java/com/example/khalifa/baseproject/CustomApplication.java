package com.example.khalifa.baseproject;

import android.app.Application;

import com.example.khalifa.baseproject.dependencyInjection.component.ApplicationComponent;
import com.example.khalifa.baseproject.dependencyInjection.component.DaggerApplicationComponent;
import com.example.khalifa.baseproject.dependencyInjection.module.ApplicationModule;
import com.example.khalifa.baseproject.dependencyInjection.module.InterActorsModule;
import com.example.khalifa.baseproject.dependencyInjection.module.NetworkModule;

public class CustomApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null)
            applicationComponent = DaggerApplicationComponent
                    .builder()
                    .interActorsModule(new InterActorsModule())
                    .networkModule(new NetworkModule(getApplicationContext()))
                    .applicationModule(new ApplicationModule(this))
                    .build();
        return applicationComponent;
    }
}
