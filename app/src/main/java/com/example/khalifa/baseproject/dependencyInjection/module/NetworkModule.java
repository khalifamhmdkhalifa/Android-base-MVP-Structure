package com.example.khalifa.baseproject.dependencyInjection.module;

import android.content.Context;
import android.util.Log;

import com.example.khalifa.baseproject.model.Constants;
import com.example.khalifa.baseproject.utilities.NetworkUtilities;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    private static final int MAX_STALE_DAYS = 300;
    private static final String HEADER_CACHE_CONTROL = "Cache-Control";
    private static final String HEADER_PRAGMA = "Pragma";
    private static final int CACHE_SIZE = 10 * 1024 * 1024; //10MB
    private Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private OkHttpClient getOkHttpClient() {
        File httpCacheDirectory = context.getCacheDir();
        Cache cache = new Cache(httpCacheDirectory, CACHE_SIZE);
        OkHttpClient client = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .addInterceptor(getLoggingInterceptor())
                .build();
        return client;
    }

    private Interceptor getLoggingInterceptor() {
        return chain -> {
            Request request = chain.request();
            Log.i("", request.url().toString());
            Response response = chain.proceed(request);
            return response;
        };
    }

    private Interceptor provideCacheInterceptor() {
        return chain -> {
            Response response = chain.proceed(chain.request());
            CacheControl cacheControl;
            if (NetworkUtilities.isNetworkAvailable(context))
                cacheControl = new CacheControl
                        .Builder().maxAge(0, TimeUnit.SECONDS).build();
            else
                cacheControl = new CacheControl
                        .Builder().maxStale(MAX_STALE_DAYS, TimeUnit.DAYS).build();
            return response.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                    .build();
        };
    }

    private Interceptor provideOfflineCacheInterceptor() {
        return chain -> {
            Request request = chain.request();
            if (!(NetworkUtilities.isNetworkAvailable(context))) {
                CacheControl cacheControl = new CacheControl
                        .Builder().maxStale(MAX_STALE_DAYS, TimeUnit.DAYS).build();
                request = request.newBuilder().removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL).cacheControl(cacheControl)
                        .build();
            }
            return chain.proceed(request);
        };
    }

}