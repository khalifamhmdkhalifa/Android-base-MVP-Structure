package com.example.khalifa.baseproject.presenter;

import android.content.Intent;

import com.example.khalifa.baseproject.view.interfaces.MainView;

public class MainActivityPresenterImpl extends MainActivityPresenter {

    public MainActivityPresenterImpl(MainView view) {
        super(view);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}
