package com.example.khalifa.baseproject.dependencyInjection.component;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.dependencyInjection.module.ApplicationModule;
import com.example.khalifa.baseproject.dependencyInjection.module.InterActorsModule;
import com.example.khalifa.baseproject.dependencyInjection.module.NetworkModule;
import com.example.khalifa.baseproject.model.interactor.MainInterActorImpl;
import com.example.khalifa.baseproject.presenter.MainActivityPresenterImpl;
import com.example.khalifa.baseproject.view.activities.ActivityNavigator;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, InterActorsModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(MainActivityPresenterImpl mainActivityPresenter);

    void inject(MainInterActorImpl interactor);

    CustomApplication provideCustomApplication();

    ActivityNavigator provideActivityNavigator();

}
