package com.example.khalifa.baseproject.presenter;

import com.example.khalifa.baseproject.model.interactor.BaseInterActor;
import com.example.khalifa.baseproject.view.interfaces.BaseActivityView;

public abstract class BaseActivityPresenter<
        V extends BaseActivityView, I extends BaseInterActor> extends BasePresenter<V, I> {

    public BaseActivityPresenter(V view) {
        super(view);
    }

}