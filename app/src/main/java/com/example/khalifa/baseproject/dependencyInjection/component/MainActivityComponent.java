package com.example.khalifa.baseproject.dependencyInjection.component;

import com.example.khalifa.baseproject.dependencyInjection.PerActivity;
import com.example.khalifa.baseproject.dependencyInjection.module.MainActivityModule;
import com.example.khalifa.baseproject.presenter.MainActivityPresenter;
import com.example.khalifa.baseproject.view.activities.MainActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MainActivityModule.class)
public interface MainActivityComponent extends ActivityComponent {
    void inject(MainActivity mainActivity);

    MainActivityPresenter provideMainActivityPresenter();
}
