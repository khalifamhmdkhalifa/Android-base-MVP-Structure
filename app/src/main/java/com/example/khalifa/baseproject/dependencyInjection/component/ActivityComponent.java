package com.example.khalifa.baseproject.dependencyInjection.component;

import com.example.khalifa.baseproject.dependencyInjection.PerActivity;

import dagger.Component;

@Component
@PerActivity
public interface ActivityComponent {
}
