package com.example.khalifa.baseproject.view.interfaces;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;

public abstract class BaseViewSubscriber<T extends Object> implements Action {
    protected BaseView view;
    protected boolean showLoading;
    protected boolean showErrorMessage;

    public Disposable subscribeObservable(Observable<T> observable) {
        return observable.subscribe(this::onResult, this::onError,
                this::onComplete, this::onSubscribe);
    }

    public BaseViewSubscriber(BaseView view,
                              boolean showLoading, boolean showErrorMessage) {
        this.view = view;
        this.showLoading = showLoading;
        this.showErrorMessage = showErrorMessage;
    }

    protected void onError(Throwable throwable) {
        throwable.printStackTrace();
        if (showLoading)
            view.hideLoading();
        if (showErrorMessage)
            view.showErrorMessage(throwable.getMessage());
    }

    public abstract void onSuccess(T result);


    protected void onResult(T result) {
        onSuccess(result);
    }

    protected void onSubscribe(Disposable disposable) {
        if (showLoading)
            view.showLoading();
    }

    protected void onComplete() {
        if (showLoading)
            view.hideLoading();
    }
}