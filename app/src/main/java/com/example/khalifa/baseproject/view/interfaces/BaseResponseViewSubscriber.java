package com.example.khalifa.baseproject.view.interfaces;

import android.util.Log;

import com.example.khalifa.baseproject.model.Response.BaseError;
import com.example.khalifa.baseproject.model.Response.BaseResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public abstract class BaseResponseViewSubscriber<T extends BaseResponse>
        extends BaseViewSubscriber<T> {

    public BaseResponseViewSubscriber(
            BaseView view, boolean showLoading, boolean showErrorMessage) {
        super(view, showLoading, showErrorMessage);
    }

    public Disposable subscribeObservable(Observable<T> observable) {
        return observable.subscribe(this::onResult, this::onError,
                this::onComplete, this::onSubscribe);
    }


    protected void onError(ArrayList<BaseError> errors) {
        if (errors == null || errors.isEmpty())
            return;

        if (showLoading)
            view.hideLoading();
        if (showErrorMessage)
            view.showErrorMessage(errors.get(0).getErrorMSG());
        Log.e(getClass().getSimpleName(), errors.get(0).getErrorMSG());
    }

    @Override
    protected void onResult(T result) {
        if (result != null && result.isValidResult())
            onSuccess(result);
        else
            onError(result.getErrors());
    }
}