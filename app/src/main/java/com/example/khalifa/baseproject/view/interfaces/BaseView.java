package com.example.khalifa.baseproject.view.interfaces;

import android.content.Context;

public interface BaseView {
    Context getContext();

    void showLoading();

    void hideLoading();

    void showErrorMessage(String message);

    void showNoInternetView();
}