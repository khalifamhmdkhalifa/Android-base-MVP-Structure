package com.example.khalifa.baseproject.presenter;

import com.example.khalifa.baseproject.model.interactor.BaseInterActor;
import com.example.khalifa.baseproject.view.interfaces.BaseFragmentView;

public abstract class BaseFragmentPresenter<V extends BaseFragmentView, I extends BaseInterActor>
        extends BasePresenter<V, I> {
    public BaseFragmentPresenter(V view) {
        super(view);
    }
}
